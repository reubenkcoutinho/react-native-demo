'use strict'
import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  TouchableOpacity ,
} from 'react-native';
const StyleSheet = require('./components/xstylesheet.js');

export default class FirstApp extends Component{
	constructor(props) {
		super(props);
		this.state = {
		  logmessage:"Hello World"
		};
	}
	
	componentDidMount() {
		console.log("componentDidMount");
	}
	
	navigateToAnimation(){
		this.props.navigator.push({
		  id: 'second'
		})
	}
	
	navigateToPedometer(){
		this.props.navigator.push({
		  id: 'third'
		})
	}
	
	changeLogMessage(){
		this.setState({
			logmessage:"Message changed"+ new Date()
		})
	}

	render() {
	return (
	  <View style={styles.container}>
		
		<Text style={styles.status}>
		  {this.props.title}
		</Text>
		<Text style={styles.instructions}>
		  {this.state.logmessage}
		</Text>
		<TouchableOpacity  onPress={this.changeLogMessage.bind(this)}>
		  <Text>Change log message</Text>
		</TouchableOpacity >
		
		<TouchableOpacity  onPress={this.navigateToAnimation.bind(this)}>
		  <Text>Navigate to second screen</Text>
		</TouchableOpacity >
		
		<TouchableOpacity  onPress={this.navigateToPedometer.bind(this)}>
		  <Text>third screen</Text>
		</TouchableOpacity >
		
	  </View>
	);
	}
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#ffc125',
    marginBottom: 5,
  },
  status: {
    textAlign: 'center',
	ios:{color:'#000000'},
	android:{color:'#FFFF00'},
    marginBottom: 5,
  },
});