'use strict'
import React, { Component } from 'react';
import { Navigator,BackAndroid } from 'react-native';

import App from './main';
import Animation from './animation';
import Pedometer from './app';

let navigator;
export default class SimpleNavigationApp extends Component {
	
	constructor(props)
	{
		super(props)
		this.state={
			navigator:null
		}
	}
	
	componentDidMount(){
		debugger;
        BackAndroid.addEventListener('hardwareBackPress', () => {
			debugger;
            if (navigator && navigator.getCurrentRoutes().length > 1) {
                navigator.pop();
                return true;
            }
            return false;
        });
    }
	componentWillUnmount(){
        BackAndroid.removeEventListener('hardwareBackPress', () => {
			debugger;
            if (navigator && navigator.getCurrentRoutes().length > 1) {
                navigator.pop();
                return true;
            }
            return false;
        });
    }	
	
	render() {
		return (
		  <Navigator
			ref={(nav) => { navigator = nav; }}
			initialRoute={{id: 'first'}}
			renderScene={this.navigatorRenderScene}
			configureScene={this.navigatorRenderSceneConfig}/>
		);
	}

  navigatorRenderScene(route, navigator) {
    switch (route.id) {
      case 'first':
        return (<App navigator={navigator} title="first"/>);
      case 'second':
        return (<Animation navigator={navigator} />);
      case 'third':
        return (<Pedometer navigator={navigator}/>);
    }
  }
  
    navigatorRenderSceneConfig(route, routeStack) {
    switch (route.id) {
      case 'second':
       return Navigator.SceneConfigs.FloatFromBottom
      
      case 'first':
	  case 'third':
       return Navigator.SceneConfigs.FloatFromLeft
    }
  }
}

