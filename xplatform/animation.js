'use strict'
import React, { Component } from 'react';
import {
  Animated,View,Image
} from 'react-native';


const StyleSheet = require('./components/xstylesheet.js');

export default class Playground extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bounceValue: new Animated.Value(0),
    };
  }
  render() {
    return (
	<View style={styles.container}>
      <Animated.Image                         // Base: Image, Text, View
        source={{uri: 'http://i.imgur.com/XMKOH81.jpg'}}
		style={{width: 400, height: 400,
		transform: [                        // `transform` is an ordered array
            {scale: this.state.bounceValue},  // Map `bounceValue` to `scale`
          ]
		  }}
      />
	  </View>
    );
  }
  componentDidMount() {
	 /* setTimeout(()=>{*/
		this.state.bounceValue.setValue(1.5);     // Start large
		Animated.spring(                          // Base: spring, decay, timing
		  this.state.bounceValue,                 // Animate `bounceValue`
		  {
			toValue: 0.8,                         // Animate to smaller size
			friction: 1,                          // Bouncier spring
		  }
		).start();                                // Start the animation
	  /*},2000)*/
  }
}

const styles = StyleSheet.create({
  container: {
	flex: 1,
	justifyContent: 'center',
	alignItems: 'center',
    ios:{backgroundColor:'#F5FCFF'},
	android:{backgroundColor:'#7F0000'},
  },
});