'use strict'
import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  TouchableOpacity ,
} from 'react-native';
const Pedometer = require('./components/pedometer.js'),
	StyleSheet = require('./components/xstylesheet.js');

const reactNativePedometer = React.createClass({
  getInitialState() {
      return {
        startDate: null,
        endDate: null,
        numberOfSteps: 0,
        distance: 0,
        floorsAscended: 0,
        floorsDescended: 0,
        currentPace: 0,
        currentCadence: 0,
        logmessage:"",
      };
    },

  componentDidMount() {
    console.log("componentDidMount");
    this._startUpdates();
  },

  _startUpdates() {
    console.info(Pedometer);
    const today = new Date();
    today.setHours(0,0,0,0);
    if(Pedometer && Pedometer.isStepCountingAvailable)
    Pedometer.isStepCountingAvailable((isTrue)=>{
      console.log("isTrue")
      console.log(isTrue)
      if(Pedometer.startPedometerUpdatesFromDate)
        Pedometer.startPedometerUpdatesFromDate((motionData) => {
          console.log("motionData: " + motionData);
          this.setState(motionData);
          this.setState({logmessage:"counting steps"})
        });
      else{
        console.log("Can't count steps")
        this.setState({logmessage:"Can't count steps"})
      }
    })
  },
  
  navSecond(){
    this.props.navigator.push({
      id: 'second'
    })
  },

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.largeNotice}>
          {this.state.numberOfSteps}
        </Text>
        <Text style={styles.status}>
          You walked {this.state.numberOfSteps} step{this.state.numberOfSteps==1 ? '' : 's'}, or about {this.state.distance.toFixed(2)} meters.
          </Text>
          <Text style={styles.status}>
          You went up {this.state.floorsAscended} floor{this.state.floorsAscended==1 ? '' : 's'}, and down {this.state.floorsDescended}.
        </Text>
        <Text style={styles.instructions}>
          Just keep your phone in your pocket and go for a walk!
        </Text>
        
        <Text style={styles.instructions}>
          {this.state.logmessage}
        </Text>
		<TouchableOpacity  onPress={this.navSecond}>
          <Text>Navigate to second screen</Text>
        </TouchableOpacity >
      </View>
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#ffc125',
    marginBottom: 5,
  },
  status: {
    textAlign: 'center',
    color: '#000000',
    marginBottom: 5,
  },
});

module.exports = reactNativePedometer;

/*
<TextInput
          style={{height: 40,width: 150, borderColor: 'gray', borderWidth: 1}}
          value={"f0b39cf7.ngrok.io"}
        />
*/