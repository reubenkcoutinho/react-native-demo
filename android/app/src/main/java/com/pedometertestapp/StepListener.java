package com.pedometertestapp;

/**
 * Created by reuben.coutinho on 23/09/2016.
 */

public interface StepListener {
    public void step(long timeNs);
}
