/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
'use strict'
import React, { Component } from 'react';
import {
  AppRegistry,
} from 'react-native';

import SimpleNavigationApp from "./xplatform/router.js"

AppRegistry.registerComponent('PedometerTestApp', () => SimpleNavigationApp);